<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body class = "bg-info w3-grey">
<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <div class="page-header">
        <h1>Hi, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b>. Welcome to our webshop! </h1>
    </div>
    <p> 
        <a href= "V1.html" class = "btn btn-warning"> Go to main page </a>
        <a href="logout.php" class="btn btn-warning">Sign Out of Your Account</a>
        <a href="reset-password.php" class="btn btn-danger">Reset Your Password</a>
        
    </p>
</body>
</body>
</html>